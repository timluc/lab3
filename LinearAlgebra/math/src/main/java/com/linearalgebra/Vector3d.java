//Khang Tim Luc
//2032290
package com.linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double a, double b, double c){
        this.x = a;
        this.y = b;
        this.z = c;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }


public double getZ() {
    return this.z;
}

public double magnitude() {
    double power = Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2);
    return Math.sqrt(power);
}

public double dotProduct(Vector3d inp) {
  return this.x * inp.getX() + this.y * inp.getY() + this.z * inp.getZ();
}

public Vector3d add(Vector3d in){
    return new Vector3d(this.x + in.getX(), this.y + in.getY(), this.z + in.getZ());
}

}
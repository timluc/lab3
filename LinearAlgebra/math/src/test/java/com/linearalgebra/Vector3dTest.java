//Khang Tim Luc
//2032290
package com.linearalgebra;

import static org.junit.Assert.assertEquals;

import java.util.Vector;

import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;


public class Vector3dTest {
@Test
public void TestGetmethod(){
    Vector3d testVector = new Vector3d(1,2,3);
    assertEquals(2, testVector.getY(), 0);
}
    @Test  
public void TestMagnitude(){
    Vector3d testVector = new Vector3d(1.0, 2.0, 3.0);
assertEquals(Math.sqrt(14), testVector.magnitude(), 0);
}
 @Test 
 public void TestDotProduct(){
Vector3d testVector = new Vector3d(1, 1, 2);
Vector3d testVector2 = new Vector3d(2,3,4);
assertEquals(13, testVector.dotProduct(testVector2), 0);
 }    

 @Test
 public void TestAdd(){
     Vector3d testVector = new Vector3d(1, 1, 2);
     Vector3d testVector2 = new Vector3d(2, 3, 4);
     assertEquals(6, testVector.add(testVector2).getZ(), 0);
 }

}
